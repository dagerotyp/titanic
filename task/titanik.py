import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''

    df['Title'] = df['Name'].str.extract("([A-Za-z]+)\.") + "."

    title = df[df['Age'].isna()]['Title'].value_counts()
    median = df.groupby("Title")['Age'].median().astype(int)

    ans = pd.DataFrame(title)
    ans['Median'] = median
    ans = ans.reset_index()
    ans.columns = ['Title', 'Count', 'Median']
    res = ans.iloc[[0,2,1],].values.tolist()
    
    return [tuple(x) for x in res]
